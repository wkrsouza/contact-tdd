# TESTE PHP/Laravel8 - Contato

Este é um teste de PHP/Laravel8 para registro de contato e envio de e-mail.

## Instalação

Utilize o gerenciador de pacotes: [Composer](https://getcomposer.org/).
<br /><br />

Após clonar o projeto, atualize as dependências com:

```
composer update
```

Ao finalizar o processo de atualização, crie o banco de dados SQLITE:
```
touch database/database.sqlite
```

Em seguida crie a estrutura que será usada neste teste:
```
php artisan migrate
```

E por fim, habilite a criação de links simbólicos:
```
php artisan storage:link
```

## Configuração SMTP

O arquivo ".env" na raiz do projeto, foi versionado e pré-configurado para agilizar esta etapa. Complete apenas as informações referente à configuração de SMTP e MAIL_TO_ADDRESS para definir o destinatário. 
<br />

```
MAIL_USERNAME=
MAIL_PASSWORD=
MAIL_FROM_ADDRESS=
MAIL_TO_ADDRESS=
```

**_Estas informações de autenticação foram enviadas no privado, por favor, entre em contato em caso de dúvidas._**


## Testando
*As regras definidas no escopo estão descritas em app\Http\Requests\SaveContactRequest.*

Feito todas etapas acima, você pode rodas os testes unitários com:
```
vendor/bin/phpunit
```

Ou inicie o seriço com:

```
php artisan serv
```

*Acesse no navegador, localhost:[Porta informada no terminal]*


## License
[MIT](https://choosealicense.com/licenses/mit/)
