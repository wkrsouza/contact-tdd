<!DOCTYPE html>
<html>
<head>
    <title>Teste PHP com Laravel 8 - Netshow.me</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</head>
<style type="text/css">
    body
    {
        background:#f2f2f2;
    }

    h2
    {
        text-align: center;
        font-size:22px;
        margin-bottom:50px;
    }

    .section
    {
        margin-top:50px;
        padding:50px;
        background:#fff;
    }

    label span
    {
        color:#F00;
        font-size: 0.7em;
    }

    .row
    {
        margin-top: 10px;
    }
</style>
<body>
    <div class="container">
        <div class="col-md-8 section offset-md-2">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h2>Teste PHP com Laravel 8 - Netshow.me</h2>
                </div>
                <div class="panel-body">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                    @endif
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Opa!</strong> Houve algum problema com sua requisição.
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('contact.post') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <label>Nome <span>(Campo Obrigatório)</span></label>
                                <input type="text" name="nome" id="nome" class="form-control" value="" placeholder="Informe seu nome" maxlength="100" required>
                            </div>
                            <div class="col-md-6">
                                <label>E-mail <span>(Campo Obrigatório)</span></label>
                                <input type="text" name="email" id="email" class="form-control" value="" placeholder="Informe seu E-mail" maxlength="100" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Telefone <span>(Campo Obrigatório)</span></label>
                                <input type="text" name="telefone" id="telefone" class="form-control" value="" placeholder="Informe seu Telefone" required>
                            </div>
                            <div class="col-md-8">
                                <label>Anexo <span>(Campo Obrigatório)</span></label>
                                <input type="file" name="anexo" id="anexo" class="form-control" value="" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label>Mensagem <span>(Campo Obrigatório)</span></label>
                                <textarea class="form-control" id="mensagem" name="mensagem" rows="3" maxlength="700" required></textarea>
                            </div>
                        </div>
                        <div class="row justify-content-between">
                            <div class="col-md-2">
                                <button type="submit" class="btn btn-success">Enviar</button>
                            </div>
                            <div class="col-md-2">
                                <a href="/" class="alert-link btn btn-outline-info">Ver Lista</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

<script>
    $(document).ready(function()
    {
        $('#telefone').mask("(99) 99999-9999");
    });
</script>

</html>
