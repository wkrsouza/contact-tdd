<!DOCTYPE html>
<html>
<head>
    <title>Teste PHP com Laravel 8 - Netshow.me</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</head>
<style type="text/css">
    body
    {
        background:#f2f2f2;
    }

    h2
    {
        text-align: center;
        font-size:22px;
        margin-bottom:50px;
    }

    .section
    {
        margin-top:50px;
        padding:50px;
        background:#fff;
    }

    label span
    {
        color:#F00;
        font-size: 0.7em;
    }

    .row
    {
        margin-top: 10px;
    }
</style>
<body>
    <div class="container">
        <div class="col-md-12 section ">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h2>Teste PHP com Laravel 8 - Netshow.me</h2>
                </div>

                <div class="row">
                    <a href="/cadastrar" class="alert-link btn btn-outline-info">Enviar Contato</a>
                </div>

                <div class="row">
                    <div class="table-responsive">
                        @if (count($contacts) > 0)
                            <table class="table table-striped" style="width: 100%">
                                <thead>
                                <tr>
                                    <th scope="col">Nome</th>
                                    <th scope="col">E-mail</th>
                                    <th scope="col">Telefone</th>
                                    <th scope="col">Anexo</th>
                                    <th scope="col">Mensagem</th>
                                    <th scope="col">IP</th>
                                    <th scope="col">Data Envio</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($contacts as $contact)
                                        <tr>
                                            <th scope="row">{{$contact->nome}}</th>
                                            <td>{{$contact->email}}</td>
                                            <td>{{$contact->telefone}}</td>
                                            <td><a href="{{asset("storage/attachments/{$contact->anexo}")}}" target="_blank">{{$contact->anexo}}</a></td>
                                            <td>{{$contact->mensagem}}</td>
                                            <td>{{$contact->ip}}</td>
                                            <td>{{$contact->created_at}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-dark" role="alert">
                                Nenhum contato enviado. Clique no botão "Enviar Contato" para iniciar o teste.
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>


</html>
