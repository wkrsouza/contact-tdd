<!DOCTYPE html>
<html>
<head>
    <title>Teste PHP com Laravel 8 - Netshow.me</title>
</head>
<body>
    <h2>E-mail teste enviado pelo projeto Contact do Teste PHP com Laravel 8 - Netshow.me</h2>
    <p><b>Nome</b>:{{ $details['nome'] }}</p>
    <p><b>E-mail</b>:{{ $details['email'] }}</p>
    <p><b>Telefone</b>:{{ $details['telefone'] }}</p>
    <p><b>Anexo</b>:<a href="{{url("storage/attachments/{$details['anexo']}")}}" target="_blank">{{ $details['anexo'] }}</a></p>
    <p><b>Mensagem</b>:{{ $details['mensagem'] }}</p>
</body>
</html>
