<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\BrazilPhone;
use App\Rules\EmailValid;
use Illuminate\Support\Str;

class SaveContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => ['required', 'string', 'max: 100'],
            'email' => ['required', new EmailValid(), 'max: 100'],
            'telefone' => ['required', new BrazilPhone()],
            'anexo' => ['required', 'file', 'mimes:pdf,doc,docx,odt,txt', 'max: 500'],
            'mensagem' => ['required', 'string', 'max: 700'],
        ];
    }
}
