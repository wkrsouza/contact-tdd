<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;

    protected $fillable = [
        'nome',
        'email' ,
        'telefone',
        'anexo',
        'mensagem',
        'ip',
        'created_at'
    ];

    public function up()
    {
        Schema::create('contacts', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('nome', 100);
            $table->string('email', 100);
            $table->integer('telefone');
            $table->string('anexo', 255);
            $table->text('mensagem');
            $table->ipAddress('ip');
            $table->timestamps();
        });
    }
}
