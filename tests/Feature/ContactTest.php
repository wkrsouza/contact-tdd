<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use App\Models\Contact;

class ContactTest extends TestCase
{
    use DatabaseMigrations;

    public function testGetContacts()
    {
        $contact = Contact::factory()->count(2)->create();

        $response = $this->get('/contacts');

        $response->assertSee($contact[0]->nome);
    }
}
