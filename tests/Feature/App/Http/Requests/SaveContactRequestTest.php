<?php

namespace Tests\Feature\App\Http\Requests;

use Faker\Factory;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\Requests\SaveContactRequest;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class SaveContactRequestTest extends TestCase
{
    use RefreshDatabase;

     /** @var \App\Http\Requests\SaveProductRequest */
     private $rules;

     /** @var \Illuminate\Validation\Validator */
     private $validator;

    public function setUp(): void
    {
        parent::setUp();

        $this->validator = app()->get('validator');

        $this->rules = (new SaveContactRequest())->rules();
    }

    public function validationProvider()
    {
        $faker = Factory::create(Factory::DEFAULT_LOCALE);

        return [
            'fail_when_any_required_missing' => [
                'passed' => false,
                'data' => [
                    'email' => $faker->safeEmail,
                    'telefone' => $faker->phoneNumber,
                    'mensagem' => $faker->paragraph,
                ]
            ],
            'fail_when_email_invalid_1' => [
                'passed' => false,
                'data' => [
                    'nome' => $faker->name,
                    'email' => 'texto qualquer',
                    'telefone' => $faker->phoneNumber,
                    'mensagem' => $faker->paragraph,
                    'anexo' => UploadedFile::fake()->create('document.pdf', 500)
                ]
            ],
            'fail_when_email_invalid_2' => [
                'passed' => false,
                'data' => [
                    'nome' => $faker->name,
                    'email' => 'a45@com',
                    'telefone' => $faker->phoneNumber,
                    'mensagem' => $faker->paragraph,
                    'anexo' => UploadedFile::fake()->create('document.pdf', 500)
                ]
            ],
            'fail_when_telefone_invalid_1' => [
                'passed' => false,
                'data' => [
                    'nome' => $faker->name,
                    'email' => $faker->safeEmail,
                    'telefone' => '0x1497589874',
                    'mensagem' => $faker->paragraph,
                    'anexo' => UploadedFile::fake()->create('document.pdf', 500)
                ]
            ],
            'fail_when_telefone_invalid_2' => [
                'passed' => false,
                'data' => [
                    'nome' => $faker->name,
                    'email' => $faker->safeEmail,
                    'telefone' => '9999999999999',
                    'mensagem' => $faker->paragraph,
                    'anexo' => UploadedFile::fake()->create('document.pdf', 500)
                ]
            ],
            'fail_when_telefone_invalid_2' => [
                'passed' => false,
                'data' => [
                    'nome' => $faker->name,
                    'email' => $faker->safeEmail,
                    'telefone' => '123456',
                    'mensagem' => $faker->paragraph,
                    'anexo' => UploadedFile::fake()->create('document.pdf', 500)
                ]
            ],
            'fail_when_file_too_long' => [
                'passed' => false,
                'data' => [
                    'nome' => $faker->name,
                    'email' => $faker->safeEmail,
                    'telefone' => '3198765421',
                    'mensagem' => $faker->paragraph,
                    'anexo' => UploadedFile::fake()->create('document.pdf', 501)
                ]
            ],
            'success_when_all_isvalid' => [
                'passed' => true,
                'data' => [
                    'nome' => $faker->name,
                    'email' => $faker->safeEmail,
                    'telefone' => '3198765421',
                    'mensagem' => $faker->paragraph,
                    'anexo' => UploadedFile::fake()->create('document.pdf', 500)
                ]
            ],
        ];
    }

    /**
     * @test
     * @dataProvider validationProvider
     * @param bool $shouldPass
     * @param array $mockedRequestData
     */
    public function validation_results_as_expected($shouldPass, $mockedRequestData)
    {
        $this->assertEquals(
            $shouldPass,
            $this->validate($mockedRequestData)
        );
    }

    protected function validate($mockedRequestData)
    {
        return $this->validator
            ->make($mockedRequestData, $this->rules)
            ->passes();
    }
}
